const express = require('express')
const app = express()

app.get('api/', (req, res) => {
    return res.json({
        api: 'Api de escola'
    })
})
app.get('/', (req, res) => {
    return res.send('Ola mundo!')
})

app.listen(8000, () => {
    console.log('app listening on port 8000')
})